Prerequisites:
Java 7 
Jar file of the complete TestNumber Package in classpath

API Usage:
final NumberReader reader = new NumberReader(new DefaultTranslator());
final String trans = reader.translate(<number>);
<number> is of type long The long data type is a 64-bit two's complement integer. The signed long has a minimum value of -263 and a maximum value of 263-1.


API can also be invoked through command line by the following command:
java com.onprem.reader.english.NumberReader [number] from the classpath


limitations/enhancements:
Decimals are not accepted (long cannot accept decimal) - translate API in AbstractNumberReader can be overloaded  to make it work with double.
Various language transitions can be achieved by changing the enum in NumberScale,by default it supports English.