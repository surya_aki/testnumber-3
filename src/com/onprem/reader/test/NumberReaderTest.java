package com.onprem.reader.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.onprem.reader.main.NumberReader;
import com.onprem.reader.translator.DefaultTranslator;

/**
 * Test class for the NumberReader object
 *          
 */
public class NumberReaderTest {

	/**
	 * An instance of the number reader
	 */
	private transient NumberReader reader;

	/**
	 * A java.util.logging logger
	 */

	private final static Logger LOGGER = Logger.getLogger("com.onprem.reader.test.NumberReaderTest");

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		reader = new NumberReader( new DefaultTranslator());
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		if (reader != null) {
			reader = null;
		}
	}

	@Test
	public final void testTranslate() {
		long[] values = new long[] { 0, 4, 10, 12, 100, 299, 1000, 1003, 2040, -45213, 100000,
                100005, 100010, -202020, 202022, 1000000, 1000001, 10000000, 10000007,99999999};

        for (long val : values) {
        	try {
    			final String son = reader.translate(val);
				LOGGER.fine(val + "\n\r" + son + "\n\r");				
				assertNotNull(son, "The translated string should not be null!");
			} catch (Exception e) {
				LOGGER.fine("Error: " + e.getMessage());
			}

		}
	}
}
