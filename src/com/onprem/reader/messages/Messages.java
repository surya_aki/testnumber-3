package com.onprem.reader.messages;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public final class Messages {
	/**
	 * The full name and package name of the properties file.
	 */
	private static final String BUNDLE_NAME = "com.onprem.reader.messages.MessagesProperties";

	/**
	 * The resource bundle used to obtain messages.
	 */
	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

	/**
	 * Default constructor which declares this classs is a singleton
	 */
	private Messages() {
	}

	/**
	 * Gets the message represented by this key.
	 * 
	 * @param key
	 *            The key which is used to fetch the message to display.
	 * @return The string whose value is to be displayed.
	 */
	public static String getString(final String key) {
		String out = null;
		try {

			out = RESOURCE_BUNDLE.getString(key);

		} catch (MissingResourceException e) {
			out = '!' + key + '!';

		}

		return out;
	}
}
