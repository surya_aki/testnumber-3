package com.onprem.reader.core;

import com.onprem.reader.exception.NumberReaderException;

/**
 * 
 * Translator serves as a base signature for classes which need to translate
 * numbers into english language equivalents. It defines methods which are
 * significant in the number translation to verbal equivalent process.
 * 
 * */
public interface Translator {
	/**
	 * Translate a given number to its language equivalent
	 * 
	 * @param num
	 *            The number  String which requires translation
	 * @return A String representing the translated number
	 * @throws NumberReaderException
	 *             Throws a NumberReaderException if an error occurs
	 */
	public String translateNumber(String num) throws NumberReaderException;
	
	/**
	 * Translate a given number to its language equivalent
	 * 
	 * @param num
	 *            The number long which requires translation
	 * @return A String representing the translated number
	 * @throws NumberReaderException
	 *             Throws a NumberReaderException if an error occurs
	 */
	public String translateNumber(long num) throws NumberReaderException;
}
