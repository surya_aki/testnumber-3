package com.onprem.reader.core;

/**
 * This class represent the base class which contains generic methods used by
 * the number reader system to parse numbers.
*
**/

import com.onprem.reader.exception.NumberReaderException;
import com.onprem.reader.core.Translator;

public abstract class AbstractNumberReader<T extends Translator> {
	
	/**
	 * The translator used by this class
	 */
	private final transient T translator;
	
	public AbstractNumberReader(final T translator) {
		super();
		this.translator = translator;
	}
	
	/**
	 * Gets the translator associated with this NumberReader
	 * 
	 * @return the translator associated with this object.
	 */
	public final T getTranslator() {
		return translator;
	}
	
	/**
	 * Translate the number into a string
	 * 
	 * @param number
	 *            The number to be translated
	 * @return The translated form of the number.
	 * @throws NumberReaderException
	 *             Throw a NumberReaderException if an error occurs
	 */

	public abstract  String translate(long number) throws NumberReaderException;
}
