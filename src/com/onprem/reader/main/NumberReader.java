package com.onprem.reader.main;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.onprem.reader.exception.NumberReaderException;
import com.onprem.reader.translator.DefaultTranslator;
import com.onprem.reader.core.AbstractNumberReader;
import com.onprem.reader.core.Translator;
import com.onprem.reader.messages.Messages;

/**
 * NumberReader is the main class used by this package. It reads number and
 * interprets it to its english pronounciation.
 * 
 **/

public class NumberReader extends AbstractNumberReader<Translator>{
	
	/**
	 * A constant representing the key <code>enter_number</code
	 */
	private static final String ENTER_NUMBER = "enter_number";

	
	/**
	 * A java.util.logging logger
	 */

	private static final Logger LOGGER = Logger.getLogger(NumberReader.class.getName());
	
	public NumberReader(Translator translator) {
		super(translator);
	}
	
	/**
	 * Translates the given number input to an english word version
	 * 
	 * @param number
	 *            The input number passed to this method whose english
	 *            representation will be passed back to the caller
	 * @return A string representing the english representation of this number
	 * @throws NumberReaderException
	 *             Throw a NumberReaderException if an error occurs
	 */
	public final String translate(final long number) throws NumberReaderException {
		String trans = "";
		try {
			trans = getTranslator().translateNumber(number);
		} catch (Exception e) {
			throw new NumberReaderException(e);
		}	
		return trans;
	}
	
	/**
	 * Command line input method for this class. Simply type java
	 * com.onprem.reader.english.NumberReader [number] from the classpath and
	 * the english translation will be printed on the console.
	 * 
	 * @param args
	 *            The argument passed at invocation.
	 */
	
	public static void main(String... args) {
		if (args.length < 1) {
			printToConsole(Messages.getString(ENTER_NUMBER));
		} else {
			try {
				final NumberReader reader = new NumberReader(new DefaultTranslator());
				final String trans = reader.translate(Long.valueOf(args[0]));
				LOGGER.log(Level.INFO, trans);
			} catch (NumberReaderException e) {
				printToConsole("Error: " + e.getMessage());
			}
		}
	}

	private static void printToConsole(final String message) {
		LOGGER.log(Level.SEVERE, message);
	}

}
