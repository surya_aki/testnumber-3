package com.onprem.reader.translator;

import com.onprem.reader.exception.NumberReaderException;

public class DefaultTranslator extends AbstractBaseTranslator {

	static private String MINUS = "minus";

	static private String ZERO_TOKEN = "zero";

	private AbstractBaseTranslator translator = new GazillionTranslator(12);

	@Override
	public String translateNumber(String value) throws NumberReaderException{
		boolean negative = false;
		if (value.startsWith("-")) {
			negative = true;
			value = value.substring(1);
		}

		String name = translator.translateNumber(value);

		if ("".equals(name)) {
			name = ZERO_TOKEN;
		} else if (negative) {
			name = MINUS.concat(SEPARATOR).concat(name);
		}          

		return name;
	}

}
