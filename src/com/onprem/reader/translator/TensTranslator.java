package com.onprem.reader.translator;

import com.onprem.reader.exception.NumberReaderException;
import com.onprem.reader.enums.NumberConstants;

public class TensTranslator extends AbstractBaseTranslator {

        static private final String UNION_SEPARATOR = "-";

        private UnitTranslator unitTranslator = new UnitTranslator();

        @Override
        public String translateNumber(String value)throws NumberReaderException {
            StringBuilder buffer = new StringBuilder();
            boolean tensFound = false;

            int number;
            if (value.length() > 3) {
                number = Integer.valueOf(value.substring(value.length() - 3), 10);
            } else {
                number = Integer.valueOf(value, 10);
            }
            number %= 100; // keep only two digits
            if (number >= 20) {
                buffer.append(NumberConstants.TENS_TOKENS[(number / 10) - 2]);
                number %= 10;
                tensFound = true;
            } else {
                number %= 20;
            }

            if (number != 0) {
                if (tensFound) {
                    buffer.append(UNION_SEPARATOR);
                }
                buffer.append(unitTranslator.translateNumber(number));
            }

            return buffer.toString();
        }
    }

