package com.onprem.reader.translator;

import com.onprem.reader.enums.NumberScale;
import com.onprem.reader.exception.NumberReaderException;


public class GazillionTranslator extends AbstractBaseTranslator {

        private HundredTranslator hundredTranslator = new HundredTranslator();
        private AbstractBaseTranslator lowTranslator;
        private int exponent;

        public GazillionTranslator(int exponent) {
            if (exponent <= 3) {
                lowTranslator = hundredTranslator;
            } else {
                lowTranslator = new GazillionTranslator(exponent - 3);
            }
            this.exponent = exponent;
        }

        public String getToken() {
        	 return NumberScale.getName(getPartDivider());
        }

        protected AbstractBaseTranslator getHighTranslator() {
            return hundredTranslator;
        }

        protected AbstractBaseTranslator getlowTranslator() {
            return lowTranslator;
        }

        public int getPartDivider() {
            return exponent;
        }

        @Override
        public String translateNumber(String value) throws NumberReaderException {
            StringBuilder buffer = new StringBuilder();

            String high, low;
            if (value.length() < getPartDivider()) {
                high = "";
                low = value;
            } else {
                int index = value.length() - getPartDivider();
                high = value.substring(0, index);
                low = value.substring(index);
            }

            String highName = getHighTranslator().translateNumber(high);
            String lowName = getlowTranslator().translateNumber(low);

            if (!"".equals(highName)) {
                buffer.append(highName);
                buffer.append(SEPARATOR);
                buffer.append(getToken());

                if (!"".equals(lowName)) {
                    buffer.append(SEPARATOR);
                }
            }

            if (!"".equals(lowName)) {
                buffer.append(lowName);
            }

            return buffer.toString();
        }
    }

