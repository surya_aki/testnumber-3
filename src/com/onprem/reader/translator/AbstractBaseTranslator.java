package com.onprem.reader.translator;
import com.onprem.reader.core.Translator;
import com.onprem.reader.exception.NumberReaderException;


abstract public class AbstractBaseTranslator implements Translator {

    static protected final String SEPARATOR = " ";
    static protected final int NO_VALUE = -1;

    public String translateNumber(long value) throws NumberReaderException {
        return translateNumber(Long.toString(value));
    }

}
