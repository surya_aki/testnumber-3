package com.onprem.reader.translator;

import com.onprem.reader.enums.NumberScale;
import com.onprem.reader.exception.NumberReaderException;

public class HundredTranslator extends AbstractBaseTranslator {

    private int EXPONENT = 2;

    private UnitTranslator unitTranslator = new UnitTranslator();
    private TensTranslator tensTranslator = new TensTranslator();

    @Override
    public String translateNumber(String value)throws NumberReaderException {
        StringBuilder buffer = new StringBuilder();

        int number;
        if ("".equals(value)) {
            number = 0;
        } else if (value.length() > 4) {
            number = Integer.valueOf(value.substring(value.length() - 4), 10);
        } else {
            number = Integer.valueOf(value, 10);
        }
        number %= 1000; // keep at least three digits

        if (number >= 100) {
            buffer.append(unitTranslator.translateNumber(number / 100));
            buffer.append(SEPARATOR);
            buffer.append(NumberScale.getName(EXPONENT));
        }

        String tensName = tensTranslator.translateNumber(number % 100);

        if (!"".equals(tensName) && (number >= 100)) {
            buffer.append(SEPARATOR);
        }
        buffer.append(tensName);

        return buffer.toString();
    }
}
