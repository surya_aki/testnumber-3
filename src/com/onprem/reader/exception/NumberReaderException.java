package com.onprem.reader.exception;

/**
 * 
 * NumberReaderException is the default exception thrown by the Number Reader
 * classes
 * 
 **/

public class NumberReaderException extends Exception {

	/**
	 * Default Constructor for IntegerReaderException
	 */
	public NumberReaderException() {
		super();
	}

	/**
	 * Public Constructor
	 * 
	 * @param message
	 *            The message to be passed to this exception
	 */
	public NumberReaderException(final String message) {
		super(message);

	}

	/**
	 * Public Constructor
	 * 
	 * @param cause
	 *            The cause of this Exception
	 */
	public NumberReaderException(final Throwable cause) {
		super(cause);

	}

	/**
	 * Public Constructor
	 * 
	 * @param message
	 *            The message to be passed to this exception
	 * @param cause
	 *            The cause of this Exception
	 */
	public NumberReaderException(final String message, final Throwable cause) {
		super(message, cause);

	}

}
