package com.onprem.reader.enums;

/**
 * 
 * An enum used to represent the verbal descriptor for a number in the
 * english language.
 * 
 */
public enum NumberScale {

	DECILLION(33, "decillion"),
	NONILLION(30, "nonillion"),
	OCTILLION(27, "octillion"),
	SEPTILLION(24, "septillion"),
	SEXTILLION(21, "sextillion"),
	QUINTILLION(18, "quintillion"),
	QUADRILLION(15, "quadrillion"),
	TRILLION(12, "trillion"),
	BILLION(9, "billion"),
	MILLION(6, "million"),
	THOUSAND(3, "thousand"),
	HUNDRED(2, "hundred");
	
	
	private int exponent;
    private String names;

    private NumberScale(int exponent, String names) {
        this.exponent = exponent;
        this.names = names;
    }

    public int getExponent() {
        return exponent;
    }

    public String getName() {
        return names;
    }
	
	public static String getName(int exponent) {
		for (NumberScale unit : values()) {
			if (unit.getExponent() == exponent) {
				return unit.getName();
			}
		}
		return "";
	}
	
}
