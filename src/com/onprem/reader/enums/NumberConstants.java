package com.onprem.reader.enums;

public class NumberConstants {
	
    static public final String[] TENS_TOKENS = new String[] { "twenty", "thirty", "fourty",
        "fifty", "sixty", "seventy", "eighty", "ninety" };
    static public final String[] UNITS_TOKENS = new String[] { "one", "two", "three", "four",
        "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen",
        "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };

}
